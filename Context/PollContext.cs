﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.EntityFrameworkCore;
using PollWebApi.Models;

namespace PollWebApi.Context
{
    public class PollContext : DbContext
    {
        public DbSet<PollClass> Poll { get; set; }

        public DbSet<VoteClass> Vote { get; set; }

        public DbSet<PollViewClass> PollView { get; set; }

        public DbSet<OptionClass> Option { get; set; }

        public PollContext(DbContextOptions<PollContext> options) :
            base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
           /* modelBuilder.Entity<OptionClass>()
                .HasOne(p => p.Poll)
                .WithMany(b => b.options)
                .HasForeignKey(p => p.poll_id);

            modelBuilder.Entity<PollViewClass>()
                .HasOne(p => p.Poll)
                .WithMany()
                .HasForeignKey(p => p.poll_id);

            modelBuilder.Entity<VoteClass>()
                .HasOne(p => p.Option)
                .WithMany()
                .HasForeignKey(p => p.option_id);*/
        }
    }
}
